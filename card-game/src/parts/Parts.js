import HeaderPage  from'./header/HeaderPage'
import 'semantic-ui-css/semantic.min.css'
import Main from './main/Main';
function Parts() {
  return (
    <div class="container">
      <div class="row">
        <HeaderPage></HeaderPage>
      </div>
      <div class="row">
        <Main></Main>
      </div>
    </div>
  );
}

export default Parts;
